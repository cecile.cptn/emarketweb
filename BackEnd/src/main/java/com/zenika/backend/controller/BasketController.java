package com.zenika.backend.controller;

import com.zenika.backend.controller.dto.BasketDto;
import com.zenika.backend.controller.dto.OrderDto;
import com.zenika.backend.controller.dto.BasketLineDto;
import com.zenika.backend.domain.Basket;
import com.zenika.backend.domain.Order;
import com.zenika.backend.service.BasketService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class BasketController {

    private final BasketService basketService;

    public BasketController(BasketService basketService) {
        this.basketService = basketService;
    }

    @GetMapping(value = "/baskets")
    public List<BasketDto> getBaskets() {
        return basketService
                .findAll()
                .stream()
                .map(BasketDto::fromDomain)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/baskets/{id}")
    public BasketDto getBasket(@PathVariable int id) {
        return BasketDto.fromDomain(basketService.findById(id));
    }

    @PostMapping(value = "/baskets")
    public ResponseEntity<BasketDto> addBasket(@RequestBody BasketDto basketDto, UriComponentsBuilder uriBuilder) {
        Basket basket = basketService.addBasket(basketDto.toDomain().setCreatedAt(Instant.now()));
        URI uri = uriBuilder.path("/baskets/{basket_id}").buildAndExpand(basket.getId()).toUri();
        return ResponseEntity
                .created(uri)
                .body(BasketDto.fromDomain(basket));
    }

    @DeleteMapping(value = "/baskets/{id}")
    public void deleteBasket(@PathVariable Integer id) {
        basketService.deleteBasket(id);
    }

    @PostMapping(value = "/baskets/{basket_id}/product")
    public BasketLineDto addProduct(@PathVariable("basket_id") Integer basketId, @RequestBody BasketLineDto basketLineDto) {
        return BasketLineDto.fromDomain(basketService.addProductLine(basketId, basketLineDto.toDomain()));
    }

    @DeleteMapping(value = "/baskets/{basket_id}/{product_id}")
    public BasketLineDto deleteProduct(@PathVariable("basket_id") Integer basketId, @PathVariable("product_id") Integer productId) {
        return BasketLineDto.fromDomain(basketService.deleteProduct(basketId, productId));
    }

    @PostMapping(value = "baskets/{basket_id}/finalize")
    public ResponseEntity<OrderDto> validateBasket(@PathVariable("basket_id") Integer basketId,
                                                   UriComponentsBuilder uriBuilder) {
        Order order = basketService.validateBasket(basketId);
        URI uri = uriBuilder.path("/orders/{order_id}").buildAndExpand(order.getId()).toUri();
        return ResponseEntity
                .created(uri)
                .body(OrderDto.fromDomain(order));
    }
}
