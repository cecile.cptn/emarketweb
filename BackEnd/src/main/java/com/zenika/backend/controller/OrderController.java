package com.zenika.backend.controller;

import com.zenika.backend.controller.dto.OrderDto;
import com.zenika.backend.domain.Order;
import com.zenika.backend.exception.OrderNotFoundException;
import com.zenika.backend.repository.jpa.JpaOrderRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class OrderController {
    private final JpaOrderRepository orderRepository;

    public OrderController(JpaOrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @GetMapping(value = "/orders")
    public List<OrderDto> findAll() {
        return orderRepository
                .findAll()
                .stream()
                .map(OrderDto::fromDomain)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/orders/{order_id}")
    public OrderDto findById(@PathVariable("order_id") Integer orderId) {
        return OrderDto.fromDomain(orderRepository.findById(orderId).orElseThrow(() -> new OrderNotFoundException(String.valueOf(orderId))));
    }

}
