package com.zenika.backend.controller;

import com.zenika.backend.controller.dto.ProductDto;
import com.zenika.backend.service.ProductService;
import com.zenika.backend.domain.Product;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/products")
    public List<ProductDto> getProducts() {
        return productService
                .findAll()
                .stream()
                .map(ProductDto::fromDomain)
                .collect(Collectors.toList());
    }

    @GetMapping("/products/{id}")
    public ProductDto getProduct(@PathVariable("id") int id) {
        return ProductDto.fromDomain(productService.findById(id));
    }


    @PostMapping("/products")
    public ResponseEntity<ProductDto> addProduct(@RequestBody ProductDto productDto, UriComponentsBuilder uriBuilder) {
        productDto = ProductDto.fromDomain(productService.save(productDto.toDomain()));
        URI uri = uriBuilder.path("/products/{id}").buildAndExpand(productDto.getId()).toUri();
        return ResponseEntity.created(uri).body(productDto);
    }


    @DeleteMapping("/products/{idProduct}")
    public void deleteProduct(@PathVariable("idProduct") int id) {
        productService.delete(id);
    }

    @PutMapping("/products/{idProduct}")
    public ResponseEntity<ProductDto> updateProduct(@PathVariable("idProduct") int id
            , @RequestBody ProductDto productDto, UriComponentsBuilder uriBuilder) {
        productDto = ProductDto.fromDomain(productService.updateProduct(id, productDto.toDomain()));
        URI uri = uriBuilder.path("/products/{id}").buildAndExpand(productDto.getId()).toUri();
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .location(uri).body(productDto);
    }


}
