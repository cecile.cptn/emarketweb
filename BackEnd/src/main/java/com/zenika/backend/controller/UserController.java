package com.zenika.backend.controller;

import com.zenika.backend.controller.dto.OrderDto;
import com.zenika.backend.controller.dto.UserDto;
import com.zenika.backend.repository.jpa.JpaOrderRepository;
import com.zenika.backend.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserController {

    private final UserService userService;
    private final JpaOrderRepository orderRepository;

    public UserController(UserService userService, JpaOrderRepository orderRepository) {
        this.userService = userService;
        this.orderRepository = orderRepository;
    }

    @GetMapping(value = "/users")
    public List<UserDto> getUsers() {
        return userService.findAll()
                .stream()
                .map(UserDto::fromDomain)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/users/{id}")
    public UserDto getUser(@PathVariable("id") int id) {
        return UserDto.fromDomain(userService.findById(id));
    }

    @PostMapping(value = "/users")
    public UserDto addUser(@RequestBody UserDto userDto) {
        return UserDto.fromDomain(userService.save(userDto.toDomain()));
    }

    @PostMapping(value = "/users/{user_id}")
    public void deleteUser(@PathVariable("user_id") int id) {
        userService.delete(id);
    }

    @GetMapping(value = "/users/{user_id}/orders")
    public List<OrderDto> findOrderByUserId(@PathVariable("user_id") Integer userId) {
        return orderRepository.findOrderByUserId(userId)
                .stream()
                .map(OrderDto::fromDomain)
                .collect(Collectors.toList());
    }
}
