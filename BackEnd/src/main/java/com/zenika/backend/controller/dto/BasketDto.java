package com.zenika.backend.controller.dto;

import com.zenika.backend.domain.Basket;
import com.zenika.backend.domain.BasketLine;
import lombok.Data;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
public class BasketDto {
    private Integer id;
    private Integer userId;
    private Instant createdAt;
    private List<BasketLineDto> basketLineDtos;

    public static BasketDto fromDomain(Basket basket) {
        List<BasketLineDto> basketLineDtos = new ArrayList<>();
        for (BasketLine basketLine : basket.getProducts()) {
            basketLineDtos.add(BasketLineDto.fromDomain(basketLine));
        }
        return new BasketDto()
                .setId(basket.getId())
                .setUserId(basket.getUserId())
                .setCreatedAt(basket.getCreatedAt())
                .setBasketLineDtos(basketLineDtos);
    }

    public Basket toDomain() {
        return new Basket(this.id, this.userId, this.createdAt);
    }
}
