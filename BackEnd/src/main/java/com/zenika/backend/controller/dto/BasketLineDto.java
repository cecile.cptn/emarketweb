package com.zenika.backend.controller.dto;

import com.zenika.backend.domain.BasketLine;
import lombok.Data;

@Data
public class BasketLineDto {

    private ProductDto productDto;
    private int quantity;
    private int totalPrice;

    public static BasketLineDto fromDomain(BasketLine basketLine) {
        return new BasketLineDto()
                .setProductDto(ProductDto.fromDomain(basketLine.getProduct()))
                .setQuantity(basketLine.getQuantity())
                .setTotalPrice(basketLine.getTotalPrice());
    }

    public BasketLine toDomain() {
        return new BasketLine(this.productDto.toDomain(), this.getQuantity());
    }
}
