package com.zenika.backend.controller.dto;

import com.zenika.backend.domain.Order;
import com.zenika.backend.domain.OrderLine;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class OrderDto {
    private Integer id;

    private UserDto userDto;

    private List<OrderLineDto> orderLinesDto;

    private int totalPrice;

    public static OrderDto fromDomain(Order order) {
        List<OrderLineDto> orderLinesDto = new ArrayList<>();
        for (OrderLine orderLine : order.getOrderLines()) {
            orderLinesDto.add(OrderLineDto.fromDomain(orderLine));
        }
        return new OrderDto()
                .setId(order.getId())
                .setUserDto(UserDto.fromDomain(order.getUser()))
                .setOrderLinesDto(orderLinesDto)
                .setTotalPrice(order.getTotalPrice());
    }
}
