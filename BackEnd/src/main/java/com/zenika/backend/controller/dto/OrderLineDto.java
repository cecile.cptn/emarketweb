package com.zenika.backend.controller.dto;

import com.zenika.backend.domain.OrderLine;
import lombok.Data;

@Data
public class OrderLineDto {
    private ProductDto productDto;
    private int quantity;
    private int totalPriceLine;

    public static OrderLineDto fromDomain(OrderLine orderLine) {
        return new OrderLineDto()
                .setProductDto(ProductDto.fromDomain(orderLine.getProduct()))
                .setQuantity(orderLine.getQuantity())
                .setTotalPriceLine(orderLine.getTotalPriceLine());
    }
}
