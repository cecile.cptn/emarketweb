package com.zenika.backend.controller.dto;

import com.zenika.backend.domain.Product;
import lombok.Data;

@Data
public class ProductDto {
    private int id;
    private String name;
    private int price;

    public static ProductDto fromDomain(Product product) {
        return new ProductDto()
                .setId(product.getId())
                .setName(product.getName())
                .setPrice(product.getPrice());
    }

    public Product toDomain() {
        return new Product(this.id, this.name, this.price);
    }
}
