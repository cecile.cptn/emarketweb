package com.zenika.backend.controller.dto;

import com.zenika.backend.domain.User;
import lombok.Data;

@Data
public class UserDto {
    private Integer id;
    private String firstName;
    private String name;
    private String address;

    public static UserDto fromDomain(User user) {
        return new UserDto()
                .setId(user.getId())
                .setFirstName(user.getFirstName())
                .setName(user.getName())
                .setAddress(user.getAddress());
    }

    public User toDomain() {
        return new User(this.id, this.firstName, this.name, this.address);
    }

}
