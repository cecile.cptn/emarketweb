package com.zenika.backend.controller.httperror;

import com.zenika.backend.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler {

    public ControllerExceptionHandler() {
    }

    @ExceptionHandler(BasketNotFoundException.class)
    public ResponseEntity<HttpError> basketNotFound(BasketNotFoundException e) {
        HttpError httpError = new HttpError("Panier n° " + e.getMessage() + " : panier introuvable !");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(httpError);
    }

    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<HttpError> productNotFound(ProductNotFoundException e) {
        HttpError httpError = new HttpError("Produit n° " + e.getMessage() + " : produit introuvable !");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(httpError);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<HttpError> userNotFound(UserNotFoundException e) {
        HttpError httpError = new HttpError("Utilisateur n° " + e.getMessage() + " : utilisateur introuvable !");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(httpError);
    }

    @ExceptionHandler(UserHasAlreadyBasketException.class)
    public ResponseEntity<HttpError> userHasAlreadyBasket(UserHasAlreadyBasketException e) {
        HttpError httpError = new HttpError("Utilisateur n° " + e.getMessage() + " : l'utilisateur à déjà un panier !");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(httpError);
    }

    @ExceptionHandler(ProductNotFoundInBasketException.class)
    public ResponseEntity<HttpError> productNotFoundInBasket(ProductNotFoundInBasketException e) {
        HttpError httpError = new HttpError("Produit n° " + e.getMessage() + " : produit non présent dans le panier !");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(httpError);
    }

    @ExceptionHandler(OrderNotFoundException.class)
    public ResponseEntity<HttpError> orderNotFound(OrderNotFoundException e) {
        HttpError httpError = new HttpError("Utilisateur n° " + e.getMessage() + " : aucune commande trouvé !");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(httpError);
    }
}
