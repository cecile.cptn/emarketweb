package com.zenika.backend.controller.httperror;

public class HttpError {

    private String message;

    public HttpError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
