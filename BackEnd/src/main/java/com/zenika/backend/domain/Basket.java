package com.zenika.backend.domain;

import com.zenika.backend.exception.ProductNotFoundInBasketException;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity
@Table(name = "baskets")
public class Basket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer userId;

    private Instant createdAt;

    @ElementCollection
    @CollectionTable(name = "basket_lines"
            , joinColumns = @JoinColumn(name = "basket_id", referencedColumnName = "id"))
    private List<BasketLine> products = new ArrayList<>();

    public Basket() {
    }

    public Basket(Integer id, Integer userId, Instant createdAt) {
        this.id = id;
        this.userId = userId;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public List<BasketLine> getProducts() {
        return products;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setBasket(List<BasketLine> products) {
        this.products = products;
    }

    public Basket setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public BasketLine getProductLineById(int id) {
        for (BasketLine basketLine : products) {
            if (Objects.equals(basketLine.getProduct().getId(), id)) {
                return basketLine;
            }
        }
        return null;
    }

    public BasketLine addProduct(Product product, int quantity) {
        BasketLine basketLine = getProductLineById(product.getId());
        if (Objects.equals(basketLine, null)) {
            basketLine = new BasketLine(product, quantity);
            products.add(basketLine);
        } else {
            basketLine.setQuantity(basketLine.getQuantity() + quantity);
        }
        return basketLine;
    }

    public BasketLine removeProduct(Product product) {
        BasketLine basketLine = getProductLineById(product.getId());
        if (Objects.equals(basketLine, null)) {
            throw new ProductNotFoundInBasketException(String.valueOf(product.getId()));
        }
        products.remove(basketLine);
        return basketLine;
    }

    public Order validateBasket(User user) {
        List<OrderLine> orderLines = products
                .stream()
                .map((bl) -> new OrderLine(bl.getProduct(), bl.getQuantity(), bl.getTotalPrice()))
                .collect(Collectors.toList());
        int totalPrice = orderLines.stream().mapToInt(OrderLine::getTotalPriceLine).sum();
        return new Order(user, orderLines, totalPrice);
    }
}
