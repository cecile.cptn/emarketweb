package com.zenika.backend.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @ElementCollection
    @CollectionTable(name = "order_lines"
            , joinColumns = @JoinColumn(name = "order_id", referencedColumnName = "id"))
    private List<OrderLine> orderLines;

    @Column(name = "total_price")
    private int totalPrice;

    public Order() {
    }

    public Order(User user, List<OrderLine> orderLines,int totalPrice) {
        this.user = user;
        this.orderLines = orderLines;
        this.totalPrice = totalPrice;
    }


    public Integer getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public int getTotalPrice() {
        return totalPrice;
    }
}
