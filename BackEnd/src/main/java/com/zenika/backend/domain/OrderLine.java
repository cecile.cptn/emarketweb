package com.zenika.backend.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Embeddable
public class OrderLine {

    @OneToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private Product product;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "total_price_line")
    private int totalPriceLine;

    public OrderLine(Product product, int quantity, int totalPriceLine) {
        this.product = product;
        this.quantity = quantity;
        this.totalPriceLine = totalPriceLine;
    }

    public OrderLine() {
    }

    public Product getProduct() {
        return this.product;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getTotalPriceLine() {
        return quantity * product.getPrice();
    }
}
