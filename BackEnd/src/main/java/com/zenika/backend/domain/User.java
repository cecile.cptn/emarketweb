package com.zenika.backend.domain;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;

    private String name;

    private String address;

    public User() {
    }

    public User(Integer id, String firstName, String name, String address) {
        this.id = id;
        this.firstName = firstName;
        this.name = name;
        this.address = address;
    }

    public User(String firstName, String name, String address) {
        this.firstName = firstName;
        this.name = name;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
