package com.zenika.backend.exception;

public class BasketNotFoundException extends RuntimeException {

    public BasketNotFoundException() {
        super();
    }

    public BasketNotFoundException(String message) {
        super(message);
    }
}
