package com.zenika.backend.exception;

public class ProductNotFoundInBasketException extends RuntimeException {
    public ProductNotFoundInBasketException(String message) {
        super(message);
    }
}
