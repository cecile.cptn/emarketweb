package com.zenika.backend.exception;

public class UserHasAlreadyBasketException extends RuntimeException {
    public UserHasAlreadyBasketException(String message) {
        super(message);
    }
}
