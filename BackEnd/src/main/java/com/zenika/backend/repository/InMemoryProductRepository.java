package com.zenika.backend.repository;


import com.zenika.backend.service.ProductService;
import com.zenika.backend.domain.Product;
import com.zenika.backend.exception.ProductNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
public class InMemoryProductRepository implements Repository<Product> {
    StoreProperties storeProperties;
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class);

    public InMemoryProductRepository(StoreProperties storeProperties) {
        this.storeProperties = storeProperties;
        LOGGER.info("Store crée");
    }

    @Override
    public List<Product> findAll() {
        return storeProperties.getProducts();
    }

    @Override
    public Optional<Product> findById(int id) throws ProductNotFoundException {
        return storeProperties.getProducts().stream().filter(product -> product.getId() == id).findFirst();
    }

    @Override
    public Product save(Product product) {
        storeProperties.getProducts().add(new Product(storeProperties.getProducts().size() + 1, product.getName(), product.getPrice()));
        return product;
    }

    @Override
    public Product delete(Product product) {
        storeProperties.getProducts().removeIf(p -> Objects.equals(p.getId(), product.getId()));
        return product;
    }

}
