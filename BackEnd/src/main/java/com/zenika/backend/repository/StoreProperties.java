package com.zenika.backend.repository;

import com.zenika.backend.domain.Product;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "store")
public class StoreProperties {
    private final List<Product> products;

    public StoreProperties(List<Product> products) {
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }
}
