package com.zenika.backend.repository.jdbc;

import com.zenika.backend.domain.Basket;
import com.zenika.backend.domain.Product;
import com.zenika.backend.domain.BasketLine;
import com.zenika.backend.exception.BasketNotFoundException;
import com.zenika.backend.repository.Repository;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
public class JdbcBasketRepository implements Repository<Basket> {

    private static final RowMapper<Basket> BASKET_ROW_MAPPER = (rs, i) -> new Basket(rs.getInt("id")
            , rs.getInt("user_id"), rs.getTimestamp("createdAt").toInstant());

    private static final RowMapper<BasketLine> PRODUCT_LINE_ROW_MAPPER = (rs, i) ->
            new BasketLine(new Product(rs.getInt("id")
                    , rs.getString("name")
                    , rs.getInt("price"))
                    , rs.getInt("quantity"));

    private final JdbcTemplate jdbcTemplate;

    public JdbcBasketRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Basket> findAll() {
        List<Basket> baskets = jdbcTemplate.query("select * from baskets", BASKET_ROW_MAPPER);
        for (Basket basket : baskets) {
            basket.setBasket(jdbcTemplate.query("select p.id, name, price, quantity from baskets left join basket_lines bl on baskets.id = bl.basket_id left join products p on p.id = bl.product_id and bl.basket_id = baskets.id where baskets.id = ?", PRODUCT_LINE_ROW_MAPPER, basket.getId()));
        }
        return baskets;
    }

    @Override
    public Optional<Basket> findById(int id) {
        try {
            List<BasketLine> products = jdbcTemplate.query(
                    "select p.id, name, price, quantity from baskets left join basket_lines bl on baskets.id = bl.basket_id left join products p on p.id = bl.product_id and bl.basket_id = baskets.id where baskets.id = ? order by product_id",
                    PRODUCT_LINE_ROW_MAPPER, id);

            Optional<Basket> basket = Optional.ofNullable(DataAccessUtils.singleResult(jdbcTemplate.query(
                    "select * from baskets where id = ?",
                    BASKET_ROW_MAPPER, id)));
            basket.get().setBasket(products);
            return basket;
        } catch (DataAccessException e) {
            throw new BasketNotFoundException(String.valueOf(id));
        }
    }

    @Override
    public Basket save(Basket basket) {
        System.out.println(basket.getId());
        if (Objects.equals(basket.getId(), null)) {
            basket = jdbcTemplate.queryForObject("insert into baskets (createdat, user_id) values (?,?) returning *", BASKET_ROW_MAPPER, Timestamp.from(Instant.now()), basket.getUserId());
        } else {
            jdbcTemplate.update("delete from basket_lines where basket_id=?", basket.getId());
            for (BasketLine basketLines : basket.getProducts()) {
                jdbcTemplate.update("insert into basket_lines (basket_id, product_id, quantity) values (?, ?, ?)", basket.getId(), basketLines.getProduct().getId(), basketLines.getQuantity());
            }

        }
        return findById(basket.getId()).get();
    }

    @Override
    public Basket delete(Basket basket) {
        jdbcTemplate.update("delete from basket_lines where basket_id=?", basket.getId());
        jdbcTemplate.update("delete from baskets where id=?", basket.getId());
        return basket;
    }

}

