package com.zenika.backend.repository.jdbc;

import com.zenika.backend.domain.Product;
import com.zenika.backend.repository.Repository;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Primary
public class JdbcProductRepository implements Repository<Product> {

    private static final RowMapper<Product> ROW_MAPPER = (rs, i) -> new Product(rs.getInt("id"), rs.getString("name"), rs.getInt("price"));

    private final JdbcTemplate jdbcTemplate;

    public JdbcProductRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Product> findAll() {
        return jdbcTemplate.query("select * from product order by id", ROW_MAPPER);
    }

    public Optional<Product> findById(int id) {
        return Optional.ofNullable(DataAccessUtils.singleResult(jdbcTemplate.query("select * from product where id = ?", ROW_MAPPER, id)));

    }

    public Product save(Product product) {
        return jdbcTemplate.queryForObject("insert into product (name,price) values (?,?) returning *", ROW_MAPPER, product.getName(), product.getPrice());
    }

    @Override
    public Product delete(Product product) {
        jdbcTemplate.update("delete from product where id = ?", product.getId());
        return product;
    }
}



