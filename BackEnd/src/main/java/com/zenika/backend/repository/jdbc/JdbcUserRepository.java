package com.zenika.backend.repository.jdbc;

import com.zenika.backend.domain.User;
import com.zenika.backend.repository.Repository;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Component
public class JdbcUserRepository implements Repository<User> {

    private final JdbcTemplate jdbcTemplate;
    private static final RowMapper<User> ROW_MAPPER = (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("firstname"), rs.getString("name"), rs.getString("address"));

    public JdbcUserRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query("select * from users", ROW_MAPPER);
    }

    @Override
    public Optional<User> findById(int id) {
        return Optional
                .ofNullable(DataAccessUtils.singleResult(jdbcTemplate.query("select * from users where id = ?", ROW_MAPPER, id)));
    }

    @Override
    public User save(User user) {
            return jdbcTemplate.queryForObject("insert into users (first_name, name, address) values (?,?,?) returning *", ROW_MAPPER, user.getFirstName(), user.getName(), user.getAddress());
    }

    @Override
    public User delete(User user) {
        return jdbcTemplate.queryForObject("delete from users where id = ?", ROW_MAPPER, user.getId());
    }

}
