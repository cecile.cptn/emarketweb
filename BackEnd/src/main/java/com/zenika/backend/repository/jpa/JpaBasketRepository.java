package com.zenika.backend.repository.jpa;

import com.zenika.backend.domain.Basket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JpaBasketRepository extends JpaRepository<Basket, Integer> {
    Optional<Basket> findBasketByUserId(int userId);

}
