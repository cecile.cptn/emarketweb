package com.zenika.backend.repository.jpa;

import com.zenika.backend.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface JpaOrderRepository extends JpaRepository<Order, Integer> {
    List<Order> findOrderByUserId(int userId);

}
