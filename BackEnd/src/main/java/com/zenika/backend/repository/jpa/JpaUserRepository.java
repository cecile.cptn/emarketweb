package com.zenika.backend.repository.jpa;

import com.zenika.backend.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;

@Repository
public interface JpaUserRepository extends JpaRepository<User, Integer> {
}
