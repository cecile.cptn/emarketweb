package com.zenika.backend.service;

import com.zenika.backend.domain.*;
import com.zenika.backend.exception.BasketNotFoundException;
import com.zenika.backend.exception.UserHasAlreadyBasketException;
import com.zenika.backend.repository.jpa.JpaBasketRepository;
import com.zenika.backend.repository.jpa.JpaOrderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BasketService {
    private final JpaBasketRepository basketRepository;
    private final JpaOrderRepository orderRepository;

    private final ProductService productService;
    private final UserService userService;


    public BasketService(JpaBasketRepository basketRepository, JpaOrderRepository orderRepository, ProductService productService, UserService userService) {
        this.basketRepository = basketRepository;
        this.orderRepository = orderRepository;
        this.productService = productService;
        this.userService = userService;
    }

    @Transactional(readOnly = true)
    public List<Basket> findAll() {
        return basketRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Basket findById(int id) {
        return basketRepository.findById(id).orElseThrow(() -> new BasketNotFoundException(String.valueOf(id)));
    }

    @Transactional
    public Basket addBasket(Basket basket) {
        User user = userService.findById(basket.getUserId());
        if (basketRepository.findBasketByUserId(user.getId()).isPresent()) {
            throw new UserHasAlreadyBasketException(String.valueOf(user.getId()));
        }
        return basketRepository.save(basket);
    }

    @Transactional
    public void deleteBasket(int id) {
        Basket basket = findById(id);
        basketRepository.delete(basket);
    }

    @Transactional
    public BasketLine addProductLine(int basketId, BasketLine basketLine) {
        Product product = productService.findById(basketLine.getProduct().getId());
        Basket basket = findById(basketId);
        return basket.addProduct(product, basketLine.getQuantity());
    }

    @Transactional
    public BasketLine deleteProduct(int basketId, int productId) {
        Product product = productService.findById(productId);
        Basket basket = findById(basketId);
        return basket.removeProduct(product);
    }

    @Transactional
    public Order validateBasket(Integer basketId) {
        Basket basket = findById(basketId);
        User user = userService.findById(basket.getUserId());
        Order order = basket.validateBasket(user);
        order = orderRepository.save(order);
        basketRepository.delete(basket);
        return order;
    }
}
