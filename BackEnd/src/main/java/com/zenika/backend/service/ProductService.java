package com.zenika.backend.service;

import com.zenika.backend.domain.Product;
import com.zenika.backend.exception.ProductNotFoundException;
import com.zenika.backend.repository.jpa.JpaProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductService {

    private final JpaProductRepository productRepository;

    public ProductService(JpaProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product findById(int id) {
        return productRepository
                .findById(id)
                .orElseThrow(() -> new ProductNotFoundException(String.valueOf(id)));
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Transactional
    public Product save(Product product) {
        return this.productRepository.save(product);
    }

    @Transactional
    public void delete(int id) {
        Product product = findById(id);
        productRepository.delete(product);
    }

    @Transactional
    public Product updateProduct(int id, Product product) {
        Product existingProduct = findById(id);
        existingProduct.setName(product.getName());
        existingProduct.setPrice(product.getPrice());
        return productRepository.save(existingProduct);
    }
}
